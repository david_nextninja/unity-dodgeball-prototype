﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public static float PLAYER_SPEED = 10.0f;
    public static float CATCHABLE_DISTANCE = 1.0f;

    public static Vector2 STAGE_SIZE = new Vector2(10, 20);

    public static float BALL_SHOOT_SPEED = 20.0f;
    public static float BALL_HEIGHT_LIMIT = 5.0f;
    public static float BALL_DEACCELERATION = 3.0f;
    public static float BALL_GRAVITY = 10.0f;
}
