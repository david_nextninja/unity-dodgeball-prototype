﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInput : MonoBehaviour
{
    private Camera cam;
    private Vector3 target;

    private PlayerManager leftPlayer;
    private PlayerManager rightPlayer;

    public void Init(GameObject left, GameObject right)
    {
        leftPlayer = left.GetComponent<PlayerManager>();
        rightPlayer = right.GetComponent<PlayerManager>();
    }

    // Use this for initialization
    private void Awake()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    private void Update()
    {
		#if UNITY_EDITOR
        target = GetMouse();
		#else
		_target = GetTouch();
		#endif

        if (target != Vector3.zero)
        {
            InvokePlayer();
            //Debug.Log(_target);
        }
    }

    private void InvokePlayer()
    {

        if (target.x > 0)
        {
            leftPlayer.Input(new Vector2(target.x, target.z));
        }
        else
        {
            rightPlayer.Input(new Vector2(target.x, target.z));
        }
    }

    private Vector3 GetTouch()
    {
        for (var i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                RaycastHit hit;
                Ray ray = cam.ScreenPointToRay(Input.GetTouch(i).position);

                if (Physics.Raycast(ray, out hit))
                    return hit.point;
            }
        }
        return Vector3.zero;
    }

    private Vector3 GetMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
                return hit.point;
        }
        return Vector3.zero;
    }
}
