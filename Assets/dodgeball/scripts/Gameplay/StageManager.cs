﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour 
{
    public GameObject[] Players = new GameObject[4];

    private TouchInput touchInput;

    // Use this for initialization
    void Awake () {
		InstantiatePlayers();
		this.touchInput = this.gameObject.AddComponent<TouchInput>();
		this.touchInput.Init(Players[0], Players[1]);
	}
	
	private void InstantiatePlayers()
	{
        var stage = Gameplay.STAGE_SIZE;
        stage.x = stage.x / 2;
        stage.y = stage.y / 2;

        for (int i = 0; i < Players.Length; i++)
		{
			if(i < Players.Length/2)
			{
                var x = Random.Range(-stage.x, stage.x);
                var z = -stage.y / 2;
                var pos = new Vector3(x, 0, z);
				this.Players[i] = Instantiate(Players[i], pos, Quaternion.Euler(Vector3.zero));
			}
			else
			{
				var x = Random.Range(-stage.x, stage.x);
                var z = stage.y / 2;
                var pos = new Vector3(x, 0, z);
				this.Players[i] = Instantiate(Players[i], pos, Quaternion.Euler(Vector3.zero));
			}
			this.Players[i].name = "Player" + i.ToString();
		}
	}
}
