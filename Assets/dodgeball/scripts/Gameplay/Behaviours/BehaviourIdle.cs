﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BehaviourIdle : Behaviour
{
    public BehaviourIdle(PlayerManager pm, Ball b) : base(pm, b)
    {
        this.replacable = true;
        this.pm = pm;
        this.ball = b;
    }

    public override void Invoke(Vector2 v) { }
}
