﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus 
{
    public bool IsMovable;
    public bool IsHoldingBall;
    public bool IsDamageable;
    public int HP;
    public int Rarity;
    public float BallDistance;
}
