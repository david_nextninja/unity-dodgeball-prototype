﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BehaviourCatch : Behaviour
{
    public BehaviourCatch(PlayerManager pm, Ball b): base(pm, b) 
	{
        this.replacable = true;
        this.pm = pm;
        this.ball = b;

		this.Catch();
    }

    public override void Invoke(Vector2 v) {}


	private void Catch()
	{
		if(this.ball.transform.parent == null)
		{
			this.ball.Catch(this.pm.holdPosition);
        	this.pm.isHoldingBall = true;
        }
    }
}
