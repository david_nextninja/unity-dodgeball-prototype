﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public abstract class Behaviour : ScriptableObject
{
    public bool replacable  { get; protected set; }
    public Ball ball { get; set; }
    public PlayerManager pm { get; set; }

    public Behaviour(PlayerManager pm, Ball b) {}

    public abstract void Invoke(Vector2 v);

    protected void ClampValues()
    {
        Vector3 pos = pm.transform.position;
        Vector2 stage = Gameplay.STAGE_SIZE;
        pos.x = Mathf.Clamp(pos.x, -stage.x / 2, stage.x / 2);
        pos.y = Mathf.Clamp(pos.y, 0, Gameplay.BALL_HEIGHT_LIMIT);
        pos.z = Mathf.Clamp(pos.z, -stage.y / 2, stage.y / 2);
        pm.transform.position = pos;
    }

    protected float GetBallDistance()
    {
        return Vector3.Distance(ball.transform.position, this.pm.transform.position);
    }
}
