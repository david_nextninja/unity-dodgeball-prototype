﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BehaviourMove : Behaviour
{
    public BehaviourMove(PlayerManager pm, Ball b): base(pm, b) 
	{
        this.replacable = true;
        this.pm = pm;
        this.ball = b;
    }

	public override void Invoke(Vector2 v)
	{
		Debug.Log("Moving!");
		Vector3 targetDir = new Vector3(v.x, 0, v.y);
        float d = Vector3.Magnitude(pm.transform.position - targetDir) * 0.05f;
        
		pm.transform.forward = targetDir;
        pm.transform.DOMove(targetDir, d).SetEase(Ease.InOutQuad);
		
		this.ClampValues();
    }
}