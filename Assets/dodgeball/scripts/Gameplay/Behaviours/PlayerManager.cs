﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public enum ControlMode {Attack, Defense, Shoot}

    public Ball ball { get; private set; }
    public Transform holdPosition { get; private set; }

    public ControlMode controlMode { get; private set; }
    public bool isMovable { get; set; }
    public bool isHoldingBall { get; set; }
    public bool isDamageable { get; set; }

    public Behaviour currentBehavior { get; protected set; }
    
    public Vector2 inputAxis { get; protected set; }

    private void Awake()
    {
        this.holdPosition = this.transform.FindChild("HoldPos");
        this.ball = GameObject.Find("Ball").GetComponent<Ball>();
    }

	private void SetBehaviour(Behaviour b)
	{
        if(currentBehavior == null)
        {
            currentBehavior = new BehaviourIdle(this, ball);
        }

        if(currentBehavior.replacable)
		{
            currentBehavior = b;
        }
	}

    private void Update()
    {
        if(currentBehavior != null)
            currentBehavior.Invoke(inputAxis);
    }

    public void Input(Vector2 v)
    {
        inputAxis = v;
        SetBehaviour(new BehaviourMove(this, ball));
    }

    public void ResetBehaviour()
    {
        SetBehaviour(new BehaviourIdle(this, ball));
    }
}
