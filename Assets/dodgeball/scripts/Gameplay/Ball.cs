﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public enum BallState { Shoot, Loose, Hold };

    public GameObject TrailEffect;

    public Transform HoldingPlayer;

    private BallState _ballState;

    private Transform _transform;
    private Vector3 _position;

    private float _speedHorizontal = 0;
    private float _speedVertical = 0;
    private float _bounceEnergy = 1.0f;

    private Vector2 _stageSize;

    // Use this for initialization
    void Awake()
    {
        _transform = this.transform;
        _stageSize = Gameplay.STAGE_SIZE;
        _ballState = BallState.Loose;
    }

    // Update is called once per frame
    void Update()
    {
        _position = _transform.position;
        CheckParent();

        if (_ballState == BallState.Loose || _ballState == BallState.Shoot)
        {
            CheckWallHit();
            Move();
        }

        if (_ballState == BallState.Loose)
        {
            DeaccelerationXZ();
            DeaccelerationY();
        }

        ClampValues();

        _transform.position = _position;

		TrailEffectControl();
    }

	private void TrailEffectControl()
	{
		if(_ballState == BallState.Shoot)
			TrailEffect.SetActive(true);
		else
			TrailEffect.SetActive(false);

	}

    private void CheckParent()
    {
        if (_transform.parent != null)
        {
            _ballState = BallState.Hold;
            Freeze();
        }
    }

    private void ClampValues()
    {
        _bounceEnergy = Mathf.Clamp(_bounceEnergy, 0, 1.0f);
        _speedHorizontal = Mathf.Clamp(_speedHorizontal, 0, 30.0f);
        _speedVertical = Mathf.Clamp(_speedVertical, -10.0f, 10.0f);

        _position.x = Mathf.Clamp(_position.x, -_stageSize.x / 2, _stageSize.x / 2);
        _position.y = Mathf.Clamp(_position.y, 0, Gameplay.BALL_HEIGHT_LIMIT);
        _position.z = Mathf.Clamp(_position.z, -_stageSize.y / 2, _stageSize.y / 2);
    }

    private void Move()
    {
        _position += _transform.forward * _speedHorizontal * Time.deltaTime;
        _position += _transform.up * _speedVertical * Time.deltaTime;
    }

    private void DeaccelerationXZ()
    {
        if (_speedHorizontal > 0)
        {
            _speedHorizontal -= Gameplay.BALL_DEACCELERATION * Time.deltaTime;
        }
        else
        {
            _speedHorizontal = 0;
        }
    }

    private void DeaccelerationY()
    {
        if (_position.y > 0)
        {
            _speedVertical -= Gameplay.BALL_GRAVITY * Time.deltaTime;
        }
		else if (_speedVertical < -5.0f && _position.y < 0.15f)
        {
            _speedVertical *= -0.5f;
        }
        else if (_position.y <= 0.05f)
        {
            _speedVertical = 0;
            _position.y = 0;
            _bounceEnergy = 1.0f;
        }
    }

    private void CheckWallHit()
    {
        Vector3 wallTop = new Vector3(0, 0, -1);
        Vector3 wallBottom = new Vector3(0, 0, 1);
        Vector3 wallLeft = new Vector3(1, 0, 0);
        Vector3 wallRight = new Vector3(-1, 0, 0);

        var ballDirection = _transform.rotation * Vector3.forward;

        // Use else if to prevent stuck
        if (_position.x >= _stageSize.x / 2)
        {
            WallBounce(ballDirection, wallLeft);
        }
        else if (_position.x <= -_stageSize.x / 2)
        {
            WallBounce(ballDirection, wallRight);
        }
        else if (_position.z >= _stageSize.y / 2)
        {
            WallBounce(ballDirection, wallTop);
        }
        else if (_position.z <= -_stageSize.y / 2)
        {
            WallBounce(ballDirection, wallBottom);
        }
    }

    private void WallBounce(Vector3 ballDirection, Vector3 wallNormal)
    {
        if (_speedHorizontal > 0)
        {
            var angle = Vector3.Reflect(ballDirection, wallNormal);
            _transform.forward = angle;

            _speedHorizontal *= 0.75f;
            _speedVertical += _speedHorizontal * 0.5f * _bounceEnergy;
            _bounceEnergy -= 0.2f;

            _ballState = BallState.Loose;
        }
    }

    public bool Throw()
    {
        if (_ballState == BallState.Hold)
        {
            _ballState = BallState.Shoot;
            _transform.parent = null;
            HoldingPlayer = null;
            _speedHorizontal = Gameplay.BALL_SHOOT_SPEED;

            return true;
        }
		else
		{
            return false;
        }
    }

    public bool Catch(Transform holdPos)
    {
        if (_ballState != BallState.Hold)
        {
			_ballState = BallState.Hold;
            this._transform.parent = holdPos;
            Freeze();
			ResetPosition();
            
			return true;
        }
		else
		{
            return false;
        }
    }

    public void Freeze()
    {
        _speedHorizontal = 0;
        _speedVertical = 0;
        _bounceEnergy = 1.0f;
    }

    public void ResetPosition()
    {
        _transform.localPosition = Vector3.zero;
        _transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
}
